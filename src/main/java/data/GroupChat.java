package data;

import java.util.ArrayList;

/**
 * Class for the GroupChat object as saved in database
 */
public class GroupChat {

    private int groupChatId;
    private String groupChatName;
    private ArrayList<Message> messageList = new ArrayList<>();
    private ArrayList<User> userList = new ArrayList<>();

    public GroupChat(int groupChatId, String groupChatName) {
        this.groupChatId = groupChatId;
        this.groupChatName = groupChatName;
    }

    public GroupChat() {

    }

    public int getGroupChatId() {
        return groupChatId;
    }

    public String getGroupChatName() {
        return groupChatName;
    }

    public ArrayList<Message> getMessageList() {
        return messageList;
    }

    public ArrayList<User> getUserList() {
        return userList;
    }

    public int setGroupChatId(int groupChatId) {
        return this.groupChatId = groupChatId;
    }

    public String setGroupChatName(String groupChatName) {
        return this.groupChatName = groupChatName;
    }

}
